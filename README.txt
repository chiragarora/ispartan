iSpartan Social Networking Portal and mobile Application Source code and documentation CD
=========================================================================================

This CD contains all the documentation and source code for our project. We have organized the CD  as follows.


cd:\-----Documentaion/--- (Report Documents)
     |			|
     |			|
     |			|------ Final Project Report.docx
     |			|
     |			|------ SE137 Final Project Report Cover.docx
     |
     |
     |--Source Code/-----
     |			|
     |			|------Mobile App/-------
     |			|			|
     |			|			|-------iSppartan Android App/ (Source Code using Android IDE)
     |			|
     |			|
     |			|
     |			|-----Web App/-----------
     |						|
     |						|-------iSpartan/-------- (Source Code using Netbeans)
     |									|
     |									|
     |									|-----images/	
     |									|
     |									|-----includes/
     |									|		
     |									|-----webServices/						
     |
     |
     |--Data Files/ (SQL dump file containing the Data base definition and content)

Group information
=================

Group Number:
------------
	Group number 4.

Members:
-------
	1) Chirag Arora
	2) Luis Barreto
	3) Banny Ng
	4) Andy Quack